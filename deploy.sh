#!/usr/bin/env bash
pwd
echo "-trying to stop container..."
sudo docker stop account-server
echo "-trying to rm container..."
sudo docker rm account-server
echo "-trying to rmi image..."
sudo docker rmi account-server
echo "-building image..."
sudo docker build -t account-server .
echo "-running image..."
#echo "-expose port 1111"
sudo docker run -d --name="account-server" account-server
#echo "-assign ip address 172.17.0.220 172.17.0.220/24@172.17.0.1"
#sudo pipework docker0 account-server 172.17.0.220/24@172.17.0.1


