package cn.ysmull.account;

public class Course {
    private String CNO;
    private String CNAME;
    private String TNO;

    public String getCNO() {
        return CNO;
    }

    public void setCNO(String CNO) {
        this.CNO = CNO;
    }

    public Course(String CNO, String CNAME, String TNO) {
        this.CNO = CNO;
        this.CNAME = CNAME;
        this.TNO = TNO;
    }

    public String getCNAME() {
        return CNAME;
    }

    public void setCNAME(String CNAME) {
        this.CNAME = CNAME;
    }

    public String getTNO() {
        return TNO;
    }

    public void setTNO(String TNO) {
        this.TNO = TNO;
    }
}
