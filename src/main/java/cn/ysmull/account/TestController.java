package cn.ysmull.account;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author maoyusu
 */
@RestController
public class TestController {

    @Autowired
    CommentService commentService;

    @RequestMapping("/hello")
    public String hello() {
        return commentService.hello();
    }
}
