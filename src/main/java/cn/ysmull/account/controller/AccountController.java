package cn.ysmull.account.controller;

import cn.ysmull.account.Course;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @author maoyusu
 */
@RestController
public class AccountController {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @RequestMapping("getall")
    public List<Course> getAll() {
        List<Course> entries = new ArrayList<>();
        entries.addAll(jdbcTemplate.query("SELECT * FROM COURSE", new Object[]{},
                (rs, row) -> new Course(rs.getString("CNO"),
                        rs.getString("CNAME"),
                        rs.getString("TNO"))));
        return entries;

    }
}
