package cn.ysmull.account;


import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author maoyusu
 */
@FeignClient("comment-service")
public interface CommentService {
    @RequestMapping("/hello")
    String hello();
}
